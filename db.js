const mysql = require('mysql');
const config = require('./config');


const con = mysql.createConnection({
    host: config.host,
    database: config.database,
    user: config.user,
    password: config.password,
    port: 3306
});
