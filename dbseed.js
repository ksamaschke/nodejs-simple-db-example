const con = require('db.js');

con.connect(function(err) {
    if (err) throw err;

    con.query('CREATE DATABASE IF NOT EXISTS ' + con.database + ';');
    con.query('USE ' + con.database + ';');
    con.query('CREATE TABLE IF NOT EXISTS cities(id int NOT NULL AUTO_INCREMENT, city varchar(100), country varchar(100), population int, PRIMARY KEY(id));', function(error, result, fields) {
        console.log(result);
    });
    con.end();
});