const mysql = require('mysql');
const config = require('./config');

class citiesDAO {

    init = function() {
        const con = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database,
            port: config.port
        });

        con.connect(function(err) {
            if (err) throw err;
        
            con.query('CREATE TABLE IF NOT EXISTS cities(id int NOT NULL AUTO_INCREMENT, city varchar(100), country varchar(100), population int, PRIMARY KEY(id));', function(error, result, fields) {
                console.log(result);
            });
            con.end();
        });
    }

    add = function(item) {
        const con = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database,
            port: config.port
        });

        if(item.city && item.country && item.population) {
            var query = 'INSERT INTO cities (city, country, population) VALUES (?,?,?)';
            con.connect(function(err) {
                if(err) throw err;

                con.query(query, [
                    item.city,
                    item.country,
                    item.population
                ])
            })
        }
    }

    read = function() {
        const con = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: config.database,
            port: config.port
        });

        return new Promise(function (resolve, reject) {
            con.connect(function(err) {
                if(err) throw err;
    
                con.query("SELECT id, city, country, population FROM cities", function(err, results) {
                    if (err) throw err;
    
                    resolve(results);
                })
            })
        })
    }

}

module.exports = citiesDAO
