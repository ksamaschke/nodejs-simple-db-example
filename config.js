const config = {};

config.host = process.env.DB_HOST;
config.database = process.env.DB_DATABASE;
config.user = process.env.DB_USER;
config.password = process.env.DB_PASSWORD;

module.exports = config;