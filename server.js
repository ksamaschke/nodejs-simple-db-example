const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const Cities = require('./citiesDAO');

const app = express();
const port = 8080;

// Where we will keep books
let books = [];

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/cities', (req, res) => {
    var cities = new Cities();

    // We will be coding here
    cities.init();

    const city = req.body;
    if(city && city.city && city.country && city.population) {
        cities.add(city).then(response => {
            res.send("Created");
        })
    } else {
        res.send("Invalid")
    }
});

app.get('/cities', (req, res) => {
    var cities = new Cities();
    cities.init();

    cities.read()
          .then(response => {
              res.send(response)
          })
})

app.listen(port, () => console.log('App listening on port ${port}!'));
